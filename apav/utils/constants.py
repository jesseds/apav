"""
This file is part of APAV.

APAV is a python package for performing analysis and visualization on
atom probe tomography data sets.

Copyright (C) 2018 Jesse Smith

APAV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

APAV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with APAV.  If not, see <http://www.gnu.org/licenses/>.
"""

import math

pi = math.pi
m0 = 9.10938356e-31  # Kg
ee = -1.6021766208e-19  # Coulomb
h = 4.135667662e-15  # eV s
h2 = 6.626070040e-34  # J s
hbar = h / (2 * pi)
hbar2 = h2 / (2 * pi)
Kb = 8.617333262145e-5  # eV/K
Kb2 = 1.380649e-22  # J/K
c = 299792458.0
