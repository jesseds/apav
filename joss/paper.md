---
title: 'APAV: An Open-Source Python Package for Mass Spectrum Analysis in Atom Probe Tomography'
tags:
  - Python
  - Atom Probe Tomography
  - Data analysis
authors:
  - name: Jesse D. Smith
    orcid: 0000-0001-7402-6658
    affiliation: 1
    corresponding: true
  - name: Marcus L. Young
    orcid: 0000-0002-4953-5562
    affiliation: 1
affiliations:
  - name: Department of Materials Science and Engineering, University of North Texas, USA
    index: 1
date: 3 June 2022
bibliography: refs.bib
---

# Summary
As microstructures are increasingly engineered with nanoscale precision, comparably precise metrological tools like Atom Probe Tomography (APT) are crucial for experimental validation. APT has the unique capability of 3D imaging and compositional/isotopic analysis at the sub-nanometer scale [@gault_review]. However, the informative potential of APT is hindered by nebulous, material-dependent evaporation physics, naturally complex data analysis/visualization, and the standards/specifications set by vendors. Transparent and accessible data analysis tools are critical for reproducible analysis of APT experiments. APAV seeks to bridge the gap of openly accessible analysis tools available to scientists leveraging APT in their research.

# Statement of need
APAV is a Python package for analysis and visualization of APT experiments, with a focus on mass spectrum analysis and detector multiple events. APAV supplies an object-oriented interface into common positional and ranged file formats, forming the foundation from which APAV or user-adapted analysis can be extended. Commercial applications dominate the field and are limited to IVAS and its successor: AP Suite. Both IVAS and AP Suite are developed by the sole commercial vendor of Local Electrode Atom Probes (LEAP). Generalized lists of open-source contributions may be found online\footnote{Such as: https://gitlab.com/jesseds/apav/-/blob/JOSS/docs/APT\%20software\%20list.md}, in addition to a collection of software-oriented journal articles [@paraprobe; @apt_control; @atomva]. APAV aims to improve the general coverage of APT tools in the Python space while specifically targeting the analysis of mass spectra.


Python is a popular language in both general and specialized computing domains, partly owing to its comprehensive centralized package management (PyPI and Conda), openness, and reputed ease of use. Such qualities have helped to ingrain Python into the greater academic community, forming our belief that the development of APT tools for Python should be pursued. Though native Python is slow, APAV uses widely accepted scientific packages for numerical acceleration (i.e., Numpy, fast-histogram, etc.), custom C extensions for performance-critical computations, and Qt for graphical interactive tools. It is sustained by a suite of unit tests, continuous integration, git version control, documentation, and is easily acquirable through the Python package index (PyPI).

# Input/output
Historically, post-reconstruction APT data is stored in two files: one file tabulating ion positions and the instantaneous experimental state (henceforth referred to as "positional" files or data), and another file mapping user-specified ion compositions to half-open intervals in the mass/charge domain (henceforth referred to as "ranging" files or data). For best interoperability, APAV does not introduce new file formats, instead supporting the major file formats: \*.pos (position), \*.epos (extended position), \*.ato (atom), \*.apt (atom probe tomography), \*.rng (range), and \*.rrng (range). A summary of the positional formats and their content is tabulated in \autoref{table:files} with further details in literature [@Larson2013; @gault_book_2012]. APAV does not implement readers or writers for pre-reconstruction data formats (\*.rhit, \*.rraw, \*.hits, \*.str, etc.) as these files are proprietary binary formats.

Table: Positional file formats with ion positions and state variables. Field inclusion is indicated by $\times$ for included, empty for not included, and $\circ$ for conditionally included. \label{table:files}

--------------------------------------------------------------------------
 Format        XYZ   m/n     TOF       DME     DC/Pulse volts    Det pos.
------------- ----- ----- --------- --------- ---------------- -----------
 POS            ×     ×      $~$       $~$           $~$            $~$
                                                 
 ePOS           ×     ×       ×         ×             ×              ×
                                                 
 ATO            ×     ×       ×        $~$            ×              ×

 APT            ×     ×    $\circ$   $\circ$       $\circ$        $\circ$
--------------------------------------------------------------------------

Positional data is stored in an `apav.Roi` container and is the primary data structure used for constructing other analysis objects. Readers and writers are accessed through its class methods `Roi.from_XXX` and `Roi.to_XXX` or convenience loading functions `apav.load_XXX` in the top-level namespace. It is worth noting that the \*.apt format is a new file introduced by Cameca in AP Suite and is the only dynamic positional format that can contain an arbitrary number of fields. Details of the \*.apt format specification can be found in the B.5.4 appendix (version 6.1) of the AP Suite Software User Guide, other formats are well summarized by @Larson2013 in appendix A. The ranging formats are stored in a `apav.RangeCollection` containers loaded by `RangeCollection.from_XXX` class methods (or `apav.load_XXX` convenience functions).


# Detector multiple events (DME)
Detector events can be partially characterized by a "multiplicity" defining the number of ions that the position sensitive detector (PSD) was able to resolve during a detection cycle (the time spanning two consecutive pulses). Ideally, all detector events would be "single", indicating a single ion was detected during the cycle. However, with increasingly nonmetallic bonding, the applied field can penetrate multiple atomic layers (due to weak electric-field screening [@Tamura2012]), resulting in both increased molecular evaporation, and the evaporation of multiple ions—a detector multiple event (DME). Analyzing the effect of DMEs is complex, too many ions reaching the PSD in quick succession (dead times are ~3ns) may result in data loss. Similarly, neutral ions produced by molecular dissociation are predominantly undetected.

![DME correlation histogram of a GdBa<sub>2</sub>Cu<sub>3</sub>O<sub>7-x</sub> superconductor with important physical phenomena annotated: co-evaporation (blue), thermally delayed evaporation (red), molecular dissociation (green), neutral generation  (purple), and kinetic energy release (lobe, yellow). \label{fig:corr_hist}](corr_window.png)

For example, a DME of four ions "ABCD" (multiplicity of four) can be expressed as six unique pairs (AB, AC, AD, BC, BD, CD). Pairing DMEs in this fashion enables analysis through correlation histograms [@Saxey2011], where the first ion is placed on the abscissa and the second on the ordinate. In APAV DME information can be accessed through `MultipleEventExtractor` that interfaces with a `Roi`—assuming the DME information exists (see \autoref{table:files}). An example of the interactive correlation histogram tool is shown in \autoref{fig:corr_hist} for a complex oxide superconductor; this particular correlation histogram exhibits all currently known physical phenomena in a correlation histogram (see @Smith2021 for details).


# Mass spectrum quantification
Mass spectrum quantification principally seeks to compute the elemental (or molecular) composition of some region of interest in the APT dataset. Conventionally, this is achieved by accumulating the number of each experimentally observed ion by segmentation of the mass spectrum into explicit bins (which can be optionally background corrected). The composition is typically expressed in terms of the *decomposed* atomic concentration, but the ionic concentration is occasionally used. APAV supports three levels of quantification, differentiated by the level background correction: 1) No correction, 2) Random noise background correction, and 3) Random + local background correction. Conventionally, the background shape is sufficiently described by a power law,

\begin{equation}
I(x)=A(x-x_0)^{-B}
\end{equation}

which is the default model used in APAV. Quantification in APAV was designed to be as flexible as possible, as the model selection and setup for one experiment may not apply to another. Further, any `Model` from `lmfit` [@lmfit] can be used (within reason) in addition to custom defined models. The automated background estimation in AP Suite and IVAS are prone to failure for complex spectra with many peaks, with no recourse available. APAV provides a flexible syntax for declaring multiple intervals for fit ranges and ion selections as needed.

![Ranged mass spectrum quantification with random noise and local background correction. \label{fig:ranged_mass}](ranged_mass.png)

For nonmetals, certain isotopes may be sensitive to DMEs, such that they are not detected in isolation. See \autoref{fig:dme_hists} for an example of DME-dependent mass spectra, generated through APAV. In these situations, it may be prudent to quantify single events separately from multiple events (DMEs) [@Yao2010]. APAV can flexibly accommodate quantification based on DME multiplicity, with most constructors taking an optional `multiplicity` argument for controlling the multiplicity parameterization. 


![Mass spectra of the first 10 DME multiplicities of a nonmetallic specimen, showing DME-dependent phenomena. \label{fig:dme_hists}](dme_hists.png)

# Delocalization and the spatial-compositional domain
APT is often considered to exhibit atomic resolution, however, this truly depends on the specimen, acquisition parameters, and the exposed surface crystallography. Nevertheless, spatial analysis is structured in terms of the point cloud, or a structured compositional grid. Construction of the compositional grid is non-trivial as the initial dataset is discretized as a list of coordinates, and each coordinate does not necessarily represent a singular elemental ion. The compositional grid is initialized by the delocalization of each ion into discrete bins using a chosen transfer function, which is then smoothed (typically using a Gaussian kernel). This smoothing is applied to mitigate aliasing artifacts possibly introduced by the grid-based sampling process of the ion distribution (and is not related the detection process itself). The amount of delocalization is defined by a specified length for each axis; i.e., $d_x$ is the delocalization distance in the x-axis, which expresses the $3\sigma$ (three standard deviations) spatial distance that each ion is "spread" along this axis. The delocalization $d_x$ constrains the standard deviations of both steps, such that they are added in quadrature: $d_x=\sqrt{(3\sigma_{x1})^2 + (3\sigma_{x2})^2}$, where $\sigma_{x1}$ and $\sigma_{x2}$ are the standard deviations of the 1st and 2nd pass stages, respectively. The delocalization distances $(d_x, d_y, d_z)$ are user-provided, but default to $3\times 3\times 1.5$ nm, meaning the delocalization distance can be different along each axis. The reasoning for the smaller delocalization in the z-axis is due to the better spatial resolution along this axis (the z-coordinate is measured by the time of flight, whereas x/y coordinates are measured by the PSD position).

The first stage (placement into bins) cannot be accelerated by means of fast Fourier transforms (FFT) or other convolution methods. Instead, ion delocalization must be computed directly, per-ion. Computations like this are difficult to express solely with NumPy, this is an example where APAV utilizes multithreaded C extensions for acceleration. For this stage, any distribution could be used to transfer ions to the grid, APAV uses a Gaussian distribution. For example, the decomposed atomic concentration of sodium $C_\text{Na}$, at the voxel centered in space at $\boldsymbol{v_i}$ amounts to,

\begin{equation}
C_{\text{Na}}(\boldsymbol{v_i}) = \frac{1}{N_i}\sum_{j\in \boldsymbol{x}}\frac{k_j}{\sigma_1\sqrt{2\pi}}\exp\left(-\frac{||\boldsymbol{p_j} - \boldsymbol{v_i}||^{2}}{2\sigma_1^{2}}\right)
\end{equation}

where $N_i$ is the total number of (decomposed) ions contained within $v_i$, $\boldsymbol{p_j}$ is the spatial position of the $j$-th ion, $x$ is the set of ions containing at least 1 Na atom, $k_j$ is the number of Na atoms constituting the ion at $\boldsymbol{p_j}$, and $\sigma_1$ is the standard deviation of the Gaussian distribution. The standard deviation is chosen such that the 3rd standard deviation is half the bin width, $3\sigma_1=b/2$; this ensures that an ion in the center of a bin mostly contributes to only that bin (typical bin widths are 0.25-2 nm). For performance reasons, the kernel is truncated after the 1st nearest neighbors (i.e., the central bin and the surrounding 26 bins).

The second stage is a Gaussian smoothing filter on each grid. For small bin sizes there may be low counts in each bin, and smoothing helps to mitigate noise. The standard deviation for this filter, $\sigma_2$, is constrained by $d=\sqrt{(3\sigma_1)^2 + (3\sigma_2)^2}$, thus $\sigma_2$ can be directly determined by $\sigma_2=\sqrt{d^2+(3\sigma_1)^2}/3$. Computation of these grids provides a basis for studying compositional variations in the spatial domain, such as 2D concentration maps, proximity histograms, multivariate histograms, etc.

# Acknowledgements
This work was performed in part at the University of North Texas's Materials Research Facility: A shared research facility for multidimensional fabrication and characterization.

# References
