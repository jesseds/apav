CALL conda activate base

CALL conda create -n apav_dist_37 -y python=3.7
CALL conda activate apav_dist_37
CALL pip install build
CALL python -m build .
CALL conda deactivate
CALL conda env remove -n apav_dist_37 -y

CALL conda create -n apav_dist_38 -y python=3.8
CALL conda activate apav_dist_38
CALL pip install build
CALL python -m build . -w
CALL conda deactivate
CALL conda env remove -n apav_dist_38 -y

CALL conda create -n apav_dist_39 -y python=3.9
CALL conda activate apav_dist_39
CALL pip install build
CALL python -m build . -w
CALL conda deactivate
CALL conda env remove -n apav_dist_39 -y

CALL conda create -n apav_dist_310 -y python=3.10
CALL conda activate apav_dist_310
CALL pip install build
CALL python -m build . -w
CALL conda deactivate
CALL conda env remove -n apav_dist_310 -y

CALL conda create -n apav_dist_311 -y python=3.11
CALL conda activate apav_dist_311
CALL pip install build
CALL python -m build . -w
CALL conda deactivate
CALL conda env remove -n apav_dist_311 -y
