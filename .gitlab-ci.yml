image: python:3.7

stages:
  - build & check
  - test
  - deploy

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  key:
  paths:
    - .cache/pip
  policy: pull-push

before_script:
  - export DISPLAY=:0.0
  - export QT_QPA_PLATFORM=minimal
  - python -m venv venv
  - source ./venv/bin/activate
  - pip install --upgrade pip
  - pip install -r requirements.txt

black:
  stage: build & check
  before_script:
  cache:
    paths:
    - .cache/pip
    policy: pull
  script:
    - pip install black
    - black --check .

install:
  stage: build & check
  cache:
    paths:
    - .cache/pip
    policy: pull
  script:
    - pip install .[dev]

docs:
  stage: build & check
  cache:
    paths:
    - .cache/pip
    policy: pull
  script:
    - apt update
    - apt install -y libgl1-mesa-glx
    - python setup.py build_ext --inplace
    - cd docs
    - make html

pytest:
  stage: test
  needs: [install]
  cache:
    paths:
    - .cache/pip
    policy: pull
  script:
    - apt update
    - apt install -y libgl1-mesa-glx
    - pip install .
    - cd venv # Change directory to avoid name clash (we want to run tests from installed package not locally)
    - python -c "from apav.tests import run_tests; run_tests()"
    - cd ..

coverage:
  stage: test
  cache:
    paths:
    - .cache/pip
    policy: pull
  needs: [install]
  script:
    - apt update
    - apt install -y libgl1-mesa-glx
    - pip install coverage coverage-badge
    - python setup.py build_ext --inplace # We need c-extensions to be compiled in order to run tests (we are running locally here)
    - coverage run -m --source=./apav pytest ./apav/tests
    - coverage report -m
    - coverage-badge -o coverage.svg
  coverage: '/^TOTAL.+?(\d+\%)$/'

dist:
  stage: deploy
  needs: [pytest]
  cache:
    paths:
      - .cache/pip
    policy: pull
  script:
    - python -m build -s -w

publish:
  stage: deploy
  needs: [pytest]
  cache:
    paths:
      - .cache/pip
    policy: pull
  script:
    - python -m build -s -w
    - twine check dist/*
