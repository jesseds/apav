Roi
===

``Roi``
-------
.. autoclass:: apav.Roi
    :members:

``RoiCylinder``
---------------
.. autoclass:: apav.RoiCylinder
    :members:

``RoiSphere``
-------------
.. autoclass:: apav.RoiSphere
    :members:

``RoiRectPrism``
----------------
.. autoclass:: apav.RoiRectPrism
    :members:
