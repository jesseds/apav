Range
=====

``Range``
---------
.. autoclass:: apav.Range
    :members:

``RangeCollection``
-------------------
.. autoclass:: apav.RangeCollection
    :members:
