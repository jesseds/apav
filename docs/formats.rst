File formats
============
APAV does not introduce its own file formats, instead using conventional formats already widespread in use. There are a few common file types for containing the experimental data from atom probe reconstructions. These are generally reduced to two kinds of files: positional data describing the atomic positions and other per-atom properties, and mass/charge ratio ranging.

.. note::
    There are a collection of other common files such as RHIT, RRAW, HITS, STR, ROOT, and others. These
    are Cameca files based on a proprietary extension of the ROOT data analysis framework. These formats are
    pre-reconstruction data, proprietary format, and are not supported.

Positional data
---------------

At a minimum, all positional formats (supported formats listed below) have at minimum the X, Y, Z, and mass/charge ratio tabulated data. Some formats extend additional fields that enable more detailed analysis.

Generally supported file formats:

* POS - (pos)itions
* ePOS - (e)xtended (pos)itions
* ATO - (a)tom probe (to)mography
* APT - (a)tom (p)robe (t)omography

Below is a table listing the positional properties of each file format. The listed fields are not inclusive, only the generally important properties are included. These are not necessarily the explicit field names either, this is to broadly show what each format contains.

============ == == == === === =============== ======= ========== ========
File format  X  Y  Z  m/n TOF Multiple events DV volt Pulse volt Det pos.
============ == == == === === =============== ======= ========== ========
POS          ✓  ✓  ✓  ✓
EPOS         ✓  ✓  ✓  ✓   ✓   ✓               ✓       ✓          ✓
ATO          ✓  ✓  ✓  ✓   ✓                   ✓       ✓          ✓
APT          ✓  ✓  ✓  ✓   ✓   ✓               ✓       ✓          ✓
============ == == == === === =============== ======= ========== ========

.. important::
    Note that only the extended pos (ePOS) and APT formats contain the information needed for multiple event analysis.
    You must save your experiment in these formats to do any form of correlated analysis.

.. _here: https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwitxanThojuAhUbOs0KHemzDqAQFjAAegQIBhAC&url=http%3A%2F%2Flink.springer.com%2Fcontent%2Fpdf%2Fbbm%253A978-1-4614-8721-0%252F1.pdf&usg=AOvVaw2a1xit5doTyR6T-LoSu-sd
    See here_ for technical details of these formats.

Below is the matrix indicating the current read/write support of each format:

=========== ==== =====
File format Read Write
=========== ==== =====
POS         ✓    ✓
EPOS        ✓    ✓
ATO         ✓
APT         ✓
RNG         ✓
RRNG        ✓
=========== ==== =====

APT format
----------
The APT format is a new format introduced by Cameca with the IVAS successor named AP Suite. Unlike its predecessors, this format is a semi-structured binary file composed of a header, optional header extension, and any number of sections each containing its structured data (as well as a section header and optional extension).

The APT format allows for a much larger variety of data to be embedded compared to the older pos/epos. Below is a list of data that is directly supported in AP Suite:

- Aperture voltage
- Apex coordinate
- Bowl-corrected TOF
- Corrected time of flight
- Delta pulse
- Detection rate
- Detector coordinates
- Event #
- FEC # (Fracture detection)
- Laser XYZ coordinates
- Laser energy
- Multiplicity (same as ions-per-pulse in ePOS)
- Pressure
- Pulse #
- Pulses since last event
- Reflectron voltage
- Specimen temperature
- Specimen voltage
- T0 subtracted TOF
- Target detection rate
- Un-corrected time of flight
- Time since last stage move
- Tip radius
- Z sphere correction

While this lists data supported directly in AP Suite, any arbitrary data may be embedded as well.

All of this ancillary data is accessed through the ``Roi.misc`` dictionary property when read in (with exception of xyz coordinates and mass/charge).

Large dataset performance considerations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use the previous table to determine what file format is best for your use case. Formats with more data are not necessarily always the best choice. If your dataset contains a large number of counts, you must consider the memory footprint required for larger files. A 75,000,000 count ePOS file will consume ~3.5Gb of memory, where the same dataset in POS format is closer to 1Gb. The POS format should be preferred unless multiple event, correlated analysis, TOF, detector coordinates, or other information is needed.

One last consideration to make when dealing with large data sets is the type of hard drive. Consider you have access to 2 computer systems, 1 fast computer with disc drives, and a slower laptop with solid state drives. If you find yourself re-running a script that loads a large dataset, you may find that the slower laptop with the solid state drive may complete the script faster - as much of the computing time may be loading the multiple Gb file.

The best solution to this may be to run your code in a Jupyter Lab notebook (https://jupyter.org/ ). This allows the script to be separated into chunks or "cells" of code that can be executed independently. In this fashion, code that loads large formats can be executed only once, and the subsequent cells for processing the dataset can be independently executed, modified, and re-executed without superfluously re-loading the dataset.

Mass ranging data
-----------------

Atom probe tomography experiments generally declare a collection of mass/charge intervals that allocate parts of the mass spectrum to specific elemental (or molecular) compositions. The number of these intervals (more commonly "ranges") can quickly increase with the complexity of the material evaporation (see complex oxides, nitrides, and carbides). Both the common ORNL and Cameca IVAS formats are supported. These files list all ranges, their composition, representative color, and volume (for reconstruction purposes).

Supported file formats:

* RNG - format originating from Oak Ridge National Lab (ORNL)
* RRNG - format originating from Cameca's IVAS software

Range files are plain text (unlike the binary positional files) and can easily be created or edited by hand. An example of a RRNG format file for a Si material with additional Cu and Cr ranges is::

    [Ions]
    Number=5
    Ion1=Si
    Ion2=Cr
    Ion3=Cu
    Ion4=C
    Ion5=O
    [Ranges]
    Number=25
    Range1=13.8745 14.2410 Vol:0.02003 Si:1 Color:CCCCCC
    Range2=27.8560 28.5950 Vol:0.02003 Si:1 Color:CCCCCC
    Range3=28.8260 29.2550 Vol:0.02003 Si:1 Color:CCCCCC
    Range4=29.7830 30.2520 Vol:0.02003 Si:1 Color:CCCCCC
    Range5=14.4070 14.6430 Vol:0.02003 Si:1 Color:CCCCCC
    Range6=14.9120 15.1710 Vol:0.02003 Si:1 Color:CCCCCC
    Range7=51.6990 54.2430 Vol:0.01201 Cr:1 Color:FF33CC
    Range8=49.6120 50.5260 Vol:0.01201 Cr:1 Color:FF33CC
    Range9=25.7710 27.2110 Vol:0.01201 Cr:1 Color:FF33CC
    Range10=24.8950 25.4450 Vol:0.01201 Cr:1 Color:FF33CC
    Range11=62.5670 63.4960 Vol:0.01181 Cu:1 Color:FF6600
    Range12=64.6190 65.5480 Vol:0.01181 Cu:1 Color:FF6600
    Range13=11.8660 12.1980 Vol:0.00878 C:1 Color:660033
    Range14=5.8960 6.1930 Vol:0.00878 C:1 Color:660033
    Range15=15.8580 16.4800 Vol:0.02883 O:1 Color:00CCFF
    Range16=17.8380 18.3040 Vol:0.02883 O:1 Color:00CCFF
    Range17=67.6220 69.5740 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range18=69.7810 70.1560 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range19=65.7600 66.2640 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range20=32.8920 33.2750 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range21=33.8980 34.8020 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range22=34.9170 35.2310 Vol:0.04083 Cr:1 O:1 Color:FF0000
    Range23=83.5950 86.5550 Vol:0.06966 Cr:1 O:2 Color:00FF00
    Range24=41.8690 43.1810 Vol:0.06966 Cr:1 O:2 Color:00FF00
    Range25=57.8190 61.1590 Vol:0.05284 Cr:2 O:1 Color:0000FF


For details on these formats (with exception of \*.apt) see Appendix A in: Larson, D. J., Prosa, T. J., Ulfig, R. M., Geiser, B. P., & Kelly, T. F. (2013). *Local Electrode Atom Probe Tomography: A User's Guide*. Springer New York. https://doi.org/10.1007/978-1-4614-8721-0. The appendix can be downloaded freely from the publisher's website.

For details on the \*.apt format see appendix B.5.3 in the AP Suite Software User Guide.
