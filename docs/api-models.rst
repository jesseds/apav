Models
======
These are extra lmfit models created for APAV.

.. currentmodule:: apav.analysis.models

``PowerLawShiftModel``
----------------------
.. autoclass:: PowerLawShiftModel
    :members:

``ExponentialGaussianModel``
----------------------------
.. autoclass:: ExponentialGaussianModel
    :members:
