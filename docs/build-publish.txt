If skipping tests:
    poetry --build publish

================ BUILD/TEST================
1) Remove any existing shells and build distributions
    poetry env remove python
    poetry build

2) Install and test sdist first
	poetry run pip install dist/{sdist_name.tar.gz}
	poetry run pytest

3) Uninstall apav and install wheel then test
	poetry env remove python
	poetry run pip install dist/{sdist_name.whl}
	poetry run pytest


================ PUBLISH ================
poetry publish