API Reference
=============

.. toctree::
    :maxdepth: 3

    api-roi
    api-range
    api-multiple-events
    api-mass-spectrum
    api-background
    api-models
    api-isotopic
    api-histogram
    api-spatial

Index
-----
* :ref:`genindex`