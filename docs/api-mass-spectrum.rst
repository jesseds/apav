Mass spectrum
=============

.. py:currentmodule:: apav.analysis.massspectrum

``CorrelationHistogram``
------------------------
.. autoclass:: CorrelationHistogram
    :members:
    :show-inheritance:
    :inherited-members:

``RangedMassSpectrum``
----------------------
.. autoclass:: RangedMassSpectrum
    :members:
    :show-inheritance:
    :inherited-members:

``NoiseCorrectedMassSpectrum``
------------------------------
.. autoclass:: NoiseCorrectedMassSpectrum
    :members:
    :show-inheritance:
    :inherited-members:

``LocalBkgCorrectedMassSpectrum``
---------------------------------
.. autoclass:: LocalBkgCorrectedMassSpectrum
    :members:
    :show-inheritance:
    :inherited-members:
