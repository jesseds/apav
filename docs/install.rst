Download and installation
=========================

APAV can be installed from the Python Package Index (PyPi) or straight from source.

PyPi
----
To install APAV from the Python Package Index::

    pip install apav

If you do not have write access, or do not want to modify the system python environment::

    pip install apav --user

to install apav for the current user.

.. important::
    Linux users should make sure they have gcc, python libraries, and system graphics libraries setup on their system. These are typically present already after a standard installation, but necessarily not for WSL, containers, or bare-bones distributions. See below for details on these situations.

Git
---
These instructions are for building apav locally, i.e., for development purposes. Move to a directory you would like to download the repository::

    cd builds/

Clone the repository::

    git clone https://gitlab.com/jesseds/apav

If you want a specific version/release::

    git checkout <release_version_here>

Move into the repository and create a new environment::

    python -m venv venv
    source venv/bin/activate

or for conda::

    conda create -n apav_env python=<desired python version>
    conda activate apav_env

Install dependencies, including the extra dependencies needed for development::

    pip install -e .[dev]

Lastly, compile the cython extensions::

    python setup.py build_ext --inplace

We can finally test that our environment is functioning::

    python -c 'from apav.tests import run_tests; run_tests()'


Optional dependencies
~~~~~~~~~~~~~~~~~~~~~
This documentation makes use of both matplotlib and jupyter notebooks to exemplify the functionalities of apav, but these packages are not strictly required. These may be installed individually or collectively by::

    pip install apav[opt]

Windows Subsystem for Linux and bare Linux systems
--------------------------------------------------
For bare or virtualized Linux systems, there may be missing OS dependencies required for apav to function. On these systems there may be 2 components missing: the gcc compiler and graphics components. For most Linux users running dedicated Linux installations, these components are likely installed during installation or inadvertently during post-installation setup. In cases where these components are not present (such as a fresh WSL install), we need to install these missing components. Below we go through the steps of installing apav on a fresh Ubuntu installation through WSL (Windows 11).

.. note::
    WSL users *must* use WSL2 and not WSL1 for functional graphics support. Windows graphics drivers should also be reasonably updated, see https://learn.microsoft.com/en-us/windows/wsl/tutorials/gui-apps

Example WSL2 setup
~~~~~~~~~~~~~~~~~~
After setting up the Ubuntu installation, update the system::

    sudo apt-get update
    sudo apt-get upgrade

We will create and activate a new virtual environment for the purposes of this example (but must install venv first)::

    sudo apt-get install python3.10-venv
    python3 -m venv venv
    source venv/bin/activate

If we proceeded to install apav by ``pip install apav``, we may receive errors related to compilation failures. We need to make sure that gcc is present on the system, and that python libraries/headers are available::

    sudo apt-get install gcc python-dev

We should now be able to install apav and successfully compile a few required cython extensions. This is simply achieved by::

    pip install apav[dev]

We have included the ``[dev]`` optional group of dependencies that is needed to run the tests, so we can eventually verify that apav is working correctly. If not running the tests, the ``[dev]`` group is not needed

At this point apav is installed, but the system is still missing some critical components and configuration allowing graphics to function. First install missing libraries for xcb, X11, and mesa::

    sudo apt-get install libgl1-mesa-glx libxkbcommon-x11-0 libxcb-icccm4 libxcb-image0 libxcb-keysyms1 libxcb-randr0 libxcb-render-util0 libxcb-xinerama0 libxcb-xfixes0 x11-utils

Finally, the display may need to be set::

    export DISPLAY=:0

We should now be able to verify that apav is working properly by running the tests::

    python -c 'from apav.tests import run_tests; run_tests()'

