Multiple events
===============

``MultipleEventExtractor``
--------------------------
.. autoclass:: apav.core.multipleevent.MultipleEventExtractor
    :members:

``pairs_per_multiplicity``
--------------------------
.. autofunction:: apav.core.multipleevent.pairs_per_multiplicity

``get_mass_indices``
--------------------
.. autofunction:: apav.core.multipleevent.get_mass_indices
