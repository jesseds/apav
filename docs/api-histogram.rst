Histograms
==========

.. currentmodule:: apav.core.histogram

``centers2edges``
-----------------
.. autofunction:: centers2edges

``histogram2d``
---------------
.. autofunction:: histogram2d

``histogram1d``
---------------
.. autofunction:: histogram1d

``histogram2d_binwidth``
------------------------
.. autofunction:: histogram2d_binwidth


