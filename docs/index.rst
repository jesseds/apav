APAV: Python for atom probe tomography
================================================

APAV (Atom Probe Analysis and Visualization) is a Python package for analysis and visualization of atom probe tomography datasets, some features:

* File read support for traditional (\*.pos, \*.epos, \*.ato, \*.rng, \*.rrng) or new (\*.apt) formats
* File write support for \*.pos and \*.epos
* Mass spectrum quantification with configurable background correction
* Calculation and configuration of correlation histograms
* Roi primitive shapes for localized analysis
* Most analyses can be parameterized by multiple-detection events
* Fast generation of the compositional grid using conventional 1st/2nd pass delocalization
* Calculation of arbitrary molecular isotopic distributions

.. _GPLv2: https://www.gnu.org/licenses/old-licenses/gpl-2.0.txt

Source code repository: https://gitlab.com/jesseds/apav

Basic usage
===========
Essential APT analysis typically involves loading some data and doing some calculation(s). Import the core functions and classes:

    >>> import apav as ap

Load an apt file from AP Suite or some other source, along with a suitable range file (or build one programtically):

    >>> roi = ap.load_apt("data/NiTiHf.apt")
    >>> rng = ap.load_rrng("data/NiTiHf_FWHM.rrng")

Now import the analysis components and compute the total (uncorrected) decomposed composition:

    >>> import apav.analysis as anl
    >>> mass = anl.RangedMassSpectrum(roi, rng, decompose=True)
    >>> mass.print()
    Ion      Composition    Counts
    -----  -------------  --------
    Ti        0.307084     2381757
    Ni        0.595634     4619772
    Hf        0.0907196     703626
    O         0.00656261     50900

Estimate the (uncorrected) composition of a grain in the upper part of the ROI using a sphere:

    >>> precip_roi = ap.RoiSphere(roi, center=(-8.5, 2.1, -20), radius=20)
    >>> precip_mass = anl.RangedMassSpectrum(precip_roi, rng, decompose=True)
    >>> precip_mass.print()
    Ion      Composition    Counts
    -----  -------------  --------
    Ti        0.397016      261053
    Ni        0.532445      350102
    Hf        0.0665544      43762
    O         0.00398457      2620

Check the documentation for more analyses and background corrections.

How to cite
===========
If you use APAV in your work, please cite::

    Smith et al., (2023). APAV: An Open-Source Python Package for Mass Spectrum Analysis
     in Atom Probe Tomography. Journal of Open Source Software, 8(83), 4862,
      https://doi.org/10.21105/joss.04862

Contents
========

.. toctree::
   :maxdepth: 2

   install
   getting_started
   formats
   roi
   ranging
   isotopic
   corr-hist
   quant
   spatial
   license
   api-reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

