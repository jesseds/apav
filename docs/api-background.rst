Background
==========

.. currentmodule:: apav.analysis.background

``Background``
--------------
.. autoclass:: Background
    :members:

``BackgroundCollection``
------------------------
.. autoclass:: BackgroundCollection
    :members:
