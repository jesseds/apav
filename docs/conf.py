# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import re

sys.path.insert(0, os.path.abspath("../"))
sys.path.insert(0, os.path.abspath("../../"))
from apav.utils.version import version_str


# -- Project information -----------------------------------------------------

project = "APAV"
copyright = "2018-2022, Jesse Smith"
author = "Jesse Smith"

# The full version, including alpha/beta/rc tags
release = version_str


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.autodoc",
    "sphinx_rtd_theme",
    "sphinx.ext.napoleon",
    "sphinx_autodoc_typehints",
    "sphinx.ext.viewcode",
    "sphinx_copybutton",
]


# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

set_type_checking_flag = False

copybutton_prompt_text = ">>> "
copybutton_only_copy_prompt_lines = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_material"
html_theme_options = {
    "globaltoc_depth": 1,
    "repo_name": "Python for APT",
    "nav_title": "APAV",
    "color_primary": "blue",
    "globaltoc_collapse": False,
    "repo_type": "gitlab",
    "color_accent": "cyan",
    "master_doc": True,
    "heroes": {
        "index": "Atom probe tomography analysis in Python",
    },
    # "html_minify": True,
    # "css_minify": True,
    "repo_url": "https://gitlab.com/jesseds/apav",
    # "base_url": "https://gitlab.com/jesseds/apav",
    "base_url": "https://apav.readthedocs.io/en/latest/",
}
pygments_style = "sphinx"
html_logo = "imgs/logo_white2.svg"
html_sidebars = {"**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]}
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']
html_extra_path = ["google9a0e44d229763c5f.html"]

# Autodoc
autoclass_content = "both"
autodoc_mock_imports = [
    "sip",
    "PyQt5",
    "PyQt5.QtGui",
    "PyQt5.QtCore",
    "PyQt5.QtWidgets",
    "PyQt5.QtOpenGL",
    "PyQt5.Qt",
    "pyqtgraph",
    "pytest-qt",
]
