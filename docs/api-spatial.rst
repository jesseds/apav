Spatial
=======

.. currentmodule:: apav.analysis.spatial

``RangedGrid``
--------------
.. autoclass:: RangedGrid
    :members:

``DensityHistogram``
--------------------
.. autoclass:: DensityHistogram
    :members:

``make_coordinate_grids``
-------------------------
.. autofunction:: make_coordinate_grids

``ion_transfer``
----------------
.. autofunction:: ion_transfer
