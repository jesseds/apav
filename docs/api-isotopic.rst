Isotopes
========

``Isotope``
-----------
.. autoclass:: apav.core.isotopic.Isotope
    :members:

``IsotopeSeries``
-------------------
.. autoclass:: apav.core.isotopic.IsotopeSeries
    :members:
